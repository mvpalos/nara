import React, {Component} from 'react';
import * as THREE from 'three';
import OrbitControls from 'three-orbitcontrols';

class Particles extends Component{
    scene = new THREE.Scene();
     

    componentDidMount(){

        /*CREATING VIEW */
    const {width, height } = this.props
    const particleCount = 10000,
          particles = new THREE.Geometry(),
          pMaterial = new THREE.PointsMaterial({
              color: 0xFFFFFF,
              size: .5
          });
    const uniforms = {
        delta: {value: 0}
    };
    const material = new THREE.ShaderMaterial({
        vertexShader: document.getElementById("vertexShader").textContent,
        fragemntShader: document.getElementById("fragmentShader").textContent
    });
    
    const scene = new THREE.Scene(),
          camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 10, 1000),
          renderer = new THREE.WebGLRenderer();

        
        /*SHAPE DETAILS  */
    
    for(let p = 0; p < particleCount; p++){
            // console.log("position=",particles)

        let particle = new THREE.Geometry();
            particle.x = Math.random() * 500 - 250;
            particle.y = Math.random() * 500 - 250;
            particle.z = Math.random() * 500 - 250;

            particle.velocity = new THREE.Vector3(0, -Math.random(), 0);
            particles.vertices.push(particle);
    }

    const particleSystem = new THREE.Points(particles, material);

        scene.add(particleSystem);

        camera.position.z = 100;

        function update(){
            particleSystem.rotation.x += Math.sin(0.001);
            particleSystem.rotation.y += Math.sin(0.001);

        let pCount = particleCount;
            while (pCount--){
            let particle = particles.vertices[pCount];
            // console.log("particle=", particle)
            if (particle.y < -10) {
                particle.y = 200;
                particle.velocity.y = Math.random();
              }

                particle.y = Math.random() * 500 - 250;
                particleSystem.geometry.__dirtyVertices = true; 
        }


                renderer.render(scene, camera);
                requestAnimationFrame(update);

            
        }

        update();

    

            /*BROWSER RESIZER */
            renderer.setSize(width , height)

            this.refs.anchor.appendChild(renderer.domElement);
            window.addEventListener("load", function() {
              let width = window.innerWidth;
              let height = window.innerHeight;
              renderer.setSize(width, height);
              camera.aspect = width/height;
              let controls = new OrbitControls(camera, renderer.domElement);
          });

          window.addEventListener("resize", function() {
			let width = window.innerWidth;
			let height = window.innerHeight;
			renderer.setSize(width, height);
			camera.aspect = width/height;
        });

        }


    render(){
        const {width, height} = this.props;
         return(
            <div class = 'canvas'>
              <div ref = 'anchor' />
            </div>
        )
    }
}
export default Particles;

