import React, {Component} from 'react';
import * as THREE from 'three';
// import OrbitControls from 'three-orbitcontrols';


class Sphere extends Component{
    scene = new THREE.Scene()
    componentDidMount(){

        /*CREATING VIEW */
    const {width, height } = this.props
    
    const scene = new THREE.Scene(),
          camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1000),
          renderer = new THREE.WebGLRenderer();

        
        /*SHAPE DETAILS */

let velocityInitMesh = new THREE.Mesh(
                new THREE.IcosahedronGeometry(1, 1),
                new THREE.MeshNormalMaterial({wireframe: true, fog: true}),
            );

        
            let light = new THREE.DirectionalLight(0Xffffff);
                light.position.set(0,1,1).normalize();

        scene.add(velocityInitMesh, light);

        camera.position.z = 5;

        function gameLoop(){
            requestAnimationFrame(gameLoop);
            velocityInitMesh.rotation.x += 0.005;
            velocityInitMesh.rotation.y += 0.01;

                renderer.render(scene, camera);
            
        }

        gameLoop();

    

            /*BROWSER RESIZER */
            renderer.setSize(width , height)

            this.refs.anchor.appendChild(renderer.domElement);
            window.addEventListener("load", function() {
              let width = window.innerWidth;
              let height = window.innerHeight;
              renderer.setSize(width, height);
              camera.aspect = width/height;
          });

          window.addEventListener("resize", function() {
			let width = window.innerWidth;
			let height = window.innerHeight;
			renderer.setSize(width, height);
			camera.aspect = width/height;
        });

        }

    render(){
    const {width, height} = this.props;
         return(
        <div>
            <div class = 'canvas'>
              <div ref = 'anchor' />
            </div>
        </div>
        )
    }
}
export default Sphere;