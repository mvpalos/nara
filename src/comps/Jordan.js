import React, {Component} from 'react';
import * as THREE from 'three';

class Jordan extends Component{
    constructor(){
        super()
    }

    render(){
         return(
        <div className = "defaultbg">
            <p>Jordan Villanueva.</p>
            <div className = "subtext">'the rational mind falls in love with its own creation'</div>
        </div>
        )
    }
}
export default Jordan;