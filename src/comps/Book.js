import React, {Component} from 'react';
import Sphere from './Sphere';
import * as THREE from 'three';

class Book extends Component{
    constructor(){
        super();
    }

    render(){
         return(
        <div>
            <h2>the novel</h2>
                <Sphere width={1650} height={850}/>            
        </div>
        )
    }
}
export default Book;